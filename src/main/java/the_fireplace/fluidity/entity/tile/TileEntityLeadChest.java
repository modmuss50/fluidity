package the_fireplace.fluidity.entity.tile;

import the_fireplace.fluidity.enums.FluidityIronChestType;

public class TileEntityLeadChest extends TileEntityFluidityIronChest {
	public TileEntityLeadChest()
	{
		super(FluidityIronChestType.LEAD);
	}
}
