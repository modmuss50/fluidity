package the_fireplace.fluidity.entity.tile;

import the_fireplace.fluidity.enums.FluidityIronChestType;

public class TileEntityTinChest extends TileEntityFluidityIronChest {
	public TileEntityTinChest()
	{
		super(FluidityIronChestType.TIN);
	}
}
