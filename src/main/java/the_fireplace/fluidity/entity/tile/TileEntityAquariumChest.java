package the_fireplace.fluidity.entity.tile;

import the_fireplace.fluidity.enums.FluidityIronChestType;

public class TileEntityAquariumChest extends TileEntityFluidityIronChest {
	public TileEntityAquariumChest()
	{
		super(FluidityIronChestType.AQUARIUM);
	}
}
