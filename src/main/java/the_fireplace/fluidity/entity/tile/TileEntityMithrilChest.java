package the_fireplace.fluidity.entity.tile;

import the_fireplace.fluidity.enums.FluidityIronChestType;

public class TileEntityMithrilChest extends TileEntityFluidityIronChest {
	public TileEntityMithrilChest()
	{
		super(FluidityIronChestType.MITHRIL);
	}
}
