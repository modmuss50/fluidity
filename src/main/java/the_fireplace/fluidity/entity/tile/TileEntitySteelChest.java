package the_fireplace.fluidity.entity.tile;

import the_fireplace.fluidity.enums.FluidityIronChestType;

public class TileEntitySteelChest extends TileEntityFluidityIronChest {
	public TileEntitySteelChest()
	{
		super(FluidityIronChestType.STEEL);
	}
}
